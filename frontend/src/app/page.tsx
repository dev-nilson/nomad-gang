import Grid from "@/components/Grid/Grid";
import Tabs from "@/components/Tabs/Tabs";
import styles from "./page.module.css";

export default function Home() {
  return (
    <main className={styles.main}>
      <Tabs
        sections={["People", "Projects"]}
        panels={[
          // eslint-disable-next-line react/jsx-key
          <Grid people={[1, 2, 3, 4, 5]} />,
          // eslint-disable-next-line react/jsx-key
          <Grid projects={[1, 2, 3]} />,
        ]}
      />
    </main>
  );
}
