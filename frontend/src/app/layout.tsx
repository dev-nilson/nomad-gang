import { Inter } from "next/font/google";
import { AuthContextProvider } from "../contexts/AuthContext";
import Navbar from "@/components/Navbar/Navbar";
import Filters from "@/components/Filters/Filters";
import "./globals.css";

const inter = Inter({ subsets: ["latin"] });

export const metadata = {
  title: "Nomad Gang",
  description: "Team up with the best",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <AuthContextProvider>
      <html lang="en">
        <body className={inter.className}>
          <Navbar />
          <div className="layout">
            <Filters />
            <div className="content">{children}</div>
          </div>
        </body>
      </html>
    </AuthContextProvider>
  );
}
