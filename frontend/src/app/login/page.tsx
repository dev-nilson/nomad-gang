import Link from "next/link";
import LoginForm from "@/components/LoginForm/LoginForm";

export default function Login() {
  return (
    <div>
      <h1>Log in to NomadGang</h1>
      <LoginForm />
      <p>
        New to NomadGang? <Link href="/signup">Create an account</Link>
      </p>
    </div>
  );
}
