import Link from "next/link";
import SignupForm from "@/components/SignupForm/SignupForm";

export default function Signup() {
  return (
    <div>
      <h1>Create an account</h1>
      <SignupForm />
      <p>
        Already have an account? <Link href="/login">Log in</Link>
      </p>
    </div>
  );
}
