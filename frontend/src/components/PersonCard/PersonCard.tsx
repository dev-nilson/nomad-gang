import avatar from "../../../public/avatar.jpeg";
import Avatar from "../Avatar/Avatar";
import styles from "./PersonCard.module.css";

export default function PersonCard() {
  return (
    <div className={styles.card}>
      <div className={styles.header}>
        <Avatar src={avatar} alt="user avatar" width={40} height={40} />
        <div>
          <h3 className={styles.name}>Name Lastname</h3>
          <p className={styles.headline}>Headline</p>
        </div>
      </div>
      <div className={styles.header}>
        <p className={styles.description}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet
          felis massa.
        </p>
      </div>
    </div>
  );
}
