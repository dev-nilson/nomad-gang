"use client";
import { useState } from "react";
import styles from "./Tabs.module.css";

type TabsProps = {
  sections: string[];
  panels: any[];
};

export default function Tabs({ sections, panels }: TabsProps) {
  const [activeTab, setActiveTab] = useState(0);

  return (
    <div className={styles.tabs}>
      <ul className={styles.sections}>
        {sections.map((section, index) => (
          <li
            className={`${styles.section} ${
              activeTab === index && styles.active
            }`}
            key={index}
            onClick={() => setActiveTab(index)}
          >
            {section}
          </li>
        ))}
      </ul>
      <div className={styles.panel}>{panels[activeTab]}</div>
    </div>
  );
}
