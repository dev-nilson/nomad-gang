import Image from "next/image";
import styles from "./Avatar.module.css";

type AvatarProps = {
  src: any;
  alt: string;
  width: number;
  height: number;
};

export default function Avatar({ src, alt, width, height }: AvatarProps) {
  return (
    <Image
      className={styles.avatar}
      src={src}
      alt={alt}
      width={width}
      height={height}
    />
  );
}
