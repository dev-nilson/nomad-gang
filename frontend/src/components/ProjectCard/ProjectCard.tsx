import Avatar from "../Avatar/Avatar";
import styles from "./ProjectCard.module.css";

export default function ProjectCard() {
  return (
    <div className={styles.card}>
      <div className={styles.header}>
        <h3 className={styles.title}>Name Lastname</h3>
      </div>
      <div className={styles.header}>
        <p className={styles.description}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet
          felis massa.
        </p>
      </div>
    </div>
  );
}
