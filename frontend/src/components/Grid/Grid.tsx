import PersonCard from "../PersonCard/PersonCard";
import ProjectCard from "../ProjectCard/ProjectCard";
import styles from "./Grid.module.css";

type GridProps = {
  people?: any[];
  projects?: any[];
};

export default function Grid({ people, projects }: GridProps) {
  return (
    <div className={styles.grid}>
      {people?.map((person) => (
        <PersonCard key={person} />
      ))}
      {projects?.map((person) => (
        <ProjectCard key={person} />
      ))}
    </div>
  );
}
