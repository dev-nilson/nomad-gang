import styles from "./Filters.module.css";

export default function Filters() {
  return (
    <div className={styles.filters}>
      <div className={styles.container}>Filters</div>
    </div>
  );
}
