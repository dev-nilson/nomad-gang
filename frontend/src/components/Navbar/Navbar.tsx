import avatar from "../../../public/avatar.jpeg";
import logo from "../../../public/logo.png";
import Image from "next/image";
import Link from "next/link";
import Avatar from "../Avatar/Avatar";
import IconButton from "../IconButton/IconButton";
import Input from "../Input/Input";
import Heart from "@/icons/Heart/Heart";
import Bell from "@/icons/Bell/Bell";
import Button from "../Button/Button";
import styles from "./Navbar.module.css";

export default function Navbar() {
  return (
    <nav className={styles.navbar}>
      <div className={styles.container}>
        <div className={styles.left}>
          <Link href="/">
            <Image
              className={styles.logo}
              src={logo}
              alt="nomdad gang logo"
              width={40}
            />
          </Link>
          <Input placeholder="Search" />
        </div>
        <div className={styles.right}>
          <div className={styles.controls}>
            <Button>New</Button>
            <IconButton icon={<Bell />} />
            <IconButton icon={<Heart />} />
          </div>
          <Avatar src={avatar} alt="user avatar" width={40} height={40} />
        </div>
      </div>
    </nav>
  );
}
