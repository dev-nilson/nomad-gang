import styles from "./IconButton.module.css";

type IconButtonProps = {
  icon: any;
};

export default function IconButton({ icon }: IconButtonProps) {
  return <button className={styles.iconButton}>{icon}</button>;
}
