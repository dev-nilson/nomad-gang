import { InputHTMLAttributes } from "react";
import styles from "./Input.module.css";

type InputProps = InputHTMLAttributes<HTMLInputElement>;

export default function Input({ children, ...props }: InputProps) {
  return <input className={styles.input} {...props} />;
}
