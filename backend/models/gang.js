import mongoose from "mongoose";

const Schema = mongoose.Schema;

const gangSchema = new Schema(
  {
    members: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
      },
    ],
  },
  { timestamps: true }
);

export default mongoose.model("Gang", gangSchema);
