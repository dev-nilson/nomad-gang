import jwt from "jsonwebtoken";
import Auth from "../models/auth.js";

const creatToken = (id) => {
  return jwt.sign({ id }, process.env.SECRET_KEY, { expiresIn: "3d" });
};

const loginUser = async (req, res) => {
  const { email, password } = req.body;

  try {
    const user = await Auth.login(email, password);
    const token = creatToken(user._id);
    res.status(200).json({ email, token });
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const signupUser = async (req, res) => {
  const { email, password, confirmation, name } = req.body;

  try {
    const user = await Auth.signup(email, password, confirmation, name);
    const token = creatToken(user._id);
    res.status(200).json({ email, token });
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

export { loginUser, signupUser };
