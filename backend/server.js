import express from "express";
import mongoose from "mongoose";
import dotenv from "dotenv";
import authRouter from "./routes/auth.js";
import userRouter from "./routes/user.js";
import gangRouter from "./routes/gang.js";

dotenv.config();
const app = express();

app.use("/api/auth", authRouter);
app.use("/api/user", userRouter);
app.use("/api/gang", gangRouter);

mongoose.set("strictQuery", false);
mongoose
  .connect(process.env.MONGO_URI)
  .then(() => {
    app.listen(process.env.PORT_NUMBER, () => {
      console.log(`Listening on port ${process.env.PORT_NUMBER}...`);
    });
  })
  .catch((err) => {
    console.log(err);
  });
